﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;

namespace CrmApi.Test
{
    public static class CrmInterop
    {
        private const string ConnectionKey = "crmConnection";

        private static IOrganizationService Service => _service ?? (_service = GetNewService());
        private static IOrganizationService _service;

        private static CrmServiceClient Connection => _connection ?? (_connection = GetNewConnection());
        private static CrmServiceClient _connection;

        private static IOrganizationService GetNewService()
            => Connection == null
            ? null
            : Connection.OrganizationWebProxyClient 
                ?? (IOrganizationService)Connection.OrganizationServiceProxy;

        private static CrmServiceClient GetNewConnection()
            => new CrmServiceClient(System.Configuration.ConfigurationManager.ConnectionStrings[ConnectionKey].ConnectionString);

        public static EntityCollection LoadAllAccounts()
        {
            CheckService();
            var accountQuery = new QueryExpression("account");
            accountQuery.ColumnSet = new ColumnSet("name", "address1_composite");
            return Service.RetrieveMultiple(accountQuery);
        }

        public static EntityCollection LoadAllContacts()
        {
            CheckService();
            var contactQuery = new QueryExpression("contact");
            contactQuery.ColumnSet = new ColumnSet(true);
            return Service.RetrieveMultiple(contactQuery);
        }

        private static void CheckService()
        {
            if (Service == null) throw Connection.LastCrmException;
        }
    }
}