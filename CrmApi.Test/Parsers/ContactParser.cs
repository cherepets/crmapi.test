﻿using CrmApi.Test.Models;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace CrmApi.Test.Parsers
{
    public static class ContactParser
    {
        public static Contact Parse(Entity entity)
            => new Contact
            {
                Id = entity.Id,
                ParentId = entity.GetAttributeValue<EntityReference>("parentcustomerid")?.Id,
                MobilePhone = entity.GetAttributeValue<string>("mobilephone"),
                Email = entity.GetAttributeValue<string>("emailaddress1")
            };

        public static IEnumerable<Contact> Parse(EntityCollection entities)
        {
            foreach (var entity in entities.Entities)
                yield return Parse(entity);
        }
    }
}