﻿using CrmApi.Test.Models;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using System;
using System.Linq;

namespace CrmApi.Test.Parsers
{
    public static class AccountParser
    {
        public static Account Parse(Entity entity, IList<Contact> contacts = null)
            => new Account
            {
                Id = entity.Id,
                Name = entity.GetAttributeValue<string>("name"),
                Address = entity.GetAttributeValue<string>("address1_composite"),
                Contacts = contacts == null
                    ? new List<Contact>()
                    : contacts.Where(c => c.ParentId == entity.Id).ToList()
            };

        public static IEnumerable<Account> Parse(EntityCollection entities, IList<Contact> contacts = null)
        {
            foreach (var entity in entities.Entities)
                yield return Parse(entity, contacts);
        }
    }
}