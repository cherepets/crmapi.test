﻿using CrmApi.Test.Models;
using CrmApi.Test.Parsers;
using System.Linq;
using System.Web.Mvc;

namespace CrmApi.Test.Controllers
{
    public class DefaultController : Controller
    {
        public ActionResult Index()
        {
            var accountEntities = CrmInterop.LoadAllAccounts();
            var contactEntities = CrmInterop.LoadAllContacts();
            var contacts = ContactParser.Parse(contactEntities).ToList();
            var accounts = AccountParser.Parse(accountEntities, contacts).ToList();
            var model = new Root
            {
                Accounts = accounts
            };
            return View(model);
        }
    }
}