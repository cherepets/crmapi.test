﻿using System;

namespace CrmApi.Test.Models
{
    public class Contact
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }

        public bool MissingInformation
            => string.IsNullOrWhiteSpace(MobilePhone)
            && string.IsNullOrWhiteSpace(Email);
    }
}