﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CrmApi.Test.Models
{
    public class Account
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public List<Contact> Contacts { get; set; }

        public bool WithoutContacts => Contacts == null || !Contacts.Any();
        public bool HasContactsWithMissingInfo => Contacts != null && Contacts.Any(c => c.MissingInformation);
    }
}