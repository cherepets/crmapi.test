﻿using System.Collections.Generic;

namespace CrmApi.Test.Models
{
    public class Root
    {
        public List<Account> Accounts { get; set; }
    }
}